﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
    public class MoviesController : Controller
    {
        // GET: Movies
        public ActionResult Random()
        {
            var movie = new Movie() { Name = "Shriek" };

            var customers = new List<Customer>
            {
                new Customer{Name = "Customer 1"},
                new Customer{Name = "Customer 2" }
            };

            var viewModel = new RandomMovieViewModel()
            {
                Movie = movie,
                Customers = customers

            };

            return View(viewModel);

        }

        public ActionResult Movie(int id)
        {
            return View(Models.Movie.movies[id]);
        }



        public ActionResult Index(int pageIndex = 1, string sortBy = "Name")
        {
            List<Movie> viewModel = Models.Movie.movies.ToList();

            switch (sortBy)
            {
                //sort movies by name
                case "Name":
                    viewModel.Sort((x, y) => x.Name.CompareTo(y.Name));
                    break;
                default:
                    break;
            }

            return View(viewModel);
        }

        [Route("movies/releases/{year}/{month}")]
        public ActionResult ByReleaseDate(int year, int month)
        {
            return Content(year + "/" + month);
        }
    }
}