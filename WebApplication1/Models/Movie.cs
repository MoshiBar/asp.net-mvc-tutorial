﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Movie
    {
        public static List<Movie> movies = new List<Movie>()
        {
            new Movie(){Id = 0, Name = "Spiderman"},
            new Movie(){Id = 1, Name = "Wall-E"},
            new Movie(){Id = 2, Name = "Shrek"}
        };


        public int Id;
        public string Name { get; set; }

    }
}