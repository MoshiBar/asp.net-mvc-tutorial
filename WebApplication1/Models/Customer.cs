﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    

    public class Customer
    {
        public static List<Customer> customers = new List<Customer>()
        {
            new Customer() { Id = 0, Name = "James Bond", movies = new List<Movie>(){ Movie.movies[0], Movie.movies[2] } },
            new Customer() { Id = 1, Name = "George Something" }
        };

        public int Id;
        public string Name;
        public List<Movie> movies = new List<Movie>();
    }
}